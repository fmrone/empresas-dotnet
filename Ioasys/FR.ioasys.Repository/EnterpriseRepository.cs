﻿using Dapper;
using FR.ioasys.Domain.Models;
using FR.ioasys.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FR.ioasys.Repository
{
    public class EnterpriseRepository : IEnterpriseReadOnlyRepository
    {
        private readonly IDbConnection _dbConnection;

        static EnterpriseRepository() => SqlMapper.AddTypeMap(typeof(string), DbType.AnsiString);

        public EnterpriseRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection ?? throw new ArgumentNullException(nameof(dbConnection));
        }

        public async Task<IEnumerable<Enterprise>> GetEnterprisesAsync()
        {
            var sql = @"SELECT CAST(Id AS VARCHAR(37)) Id, Name, Type
                          FROM Enterprise";

            var enterprises = await _dbConnection.QueryAsync<Enterprise>(sql);

            return enterprises;
        }

        public async Task<Enterprise> GetEnterpriseByIdAsync(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@id", id);

            var sql = @"SELECT CAST(Id AS VARCHAR(37)) Id, Name, Type
                          FROM Enterprise
                         WHERE Id = @id";

            var enterprise = await _dbConnection.QueryAsync<Enterprise>(sql, parameters);

            return enterprise.FirstOrDefault();
        }

        public async Task<Enterprise> GetEnterpriseByNameAndTypeAsync(string name, byte type)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@name", name);
            parameters.Add("@type", type);

            var sql = @"SELECT CAST(Id AS VARCHAR(37)) Id, Name, Type
                          FROM Enterprise
                         WHERE Name = @name
                           AND Type = @type";

            var enterprise = await _dbConnection.QueryAsync<Enterprise>(sql, parameters);

            return enterprise.FirstOrDefault();
        }
    }
}
