﻿USE [D:\DESENVOLVIMENTO\LABs\Ioasys\IOASYS.MDF]
GO

-- Criação da tabela
IF NOT EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME='Enterprise' AND XTYPE='U')
    CREATE TABLE Enterprise (
        Id UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
        [Name] VARCHAR(100) NOT NULL,
        [Type] TINYINT NOT NULL
    )
GO

-- Inserção de registros
IF NOT EXISTS (SELECT 1 FROM Enterprise WHERE Name='Ioasys')
    INSERT Enterprise (Id, Name, Type)
        SELECT NEWID(), 'ioasys', 21
GO

IF NOT EXISTS (SELECT 1 FROM Enterprise WHERE Name='Prodap')
    INSERT Enterprise (Id, Name, Type)
        SELECT NEWID(), 'Prodap', 1
GO

IF NOT EXISTS (SELECT 1 FROM Enterprise WHERE Name='Latam')
    INSERT Enterprise (Id, Name, Type)
        SELECT NEWID(), 'Latam', 2
GO

IF NOT EXISTS (SELECT 1 FROM Enterprise WHERE Name='BK')
    INSERT Enterprise (Id, Name, Type)
        SELECT NEWID(), 'BK', 9
GO
