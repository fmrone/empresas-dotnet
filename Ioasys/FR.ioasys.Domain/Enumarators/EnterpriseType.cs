﻿using System.ComponentModel;

namespace FR.ioasys.Domain.Enumarators
{
    public enum EnterpriseType
    {
        [Description("Agro")]
        Agro = 1,

        [Description("Aviation")]
        Aviation = 2,

        [Description("Biotech")]
        Biotech = 3,

        [Description("Eco")]
        Eco = 4,

        [Description("Ecommerce")]
        Ecommerce = 5,

        [Description("Education")]
        Education = 6,

        [Description("Fashion")]
        Fashion = 7,

        [Description("Fintech")]
        Fintech = 8,

        [Description("Food")]
        Food = 9,

        [Description("Games")]
        Games = 10,

        [Description("Health")]
        Health = 11,

        [Description("IOT")]
        IOT = 12,

        [Description("Logistics")]
        Logistics = 13,

        [Description("Media")]
        Media = 14,

        [Description("Mining")]
        Mining = 15,

        [Description("Products")]
        Products = 16,

        [Description("Real Estate")]
        RealEstate = 17,

        [Description("Service")]
        Service = 18,

        [Description("Smart City")]
        SmartCity = 19,

        [Description("Social")]
        Social = 20,

        [Description("Software")]
        Software = 21,

        [Description("Technology")]
        Technology = 22,

        [Description("Tourism")]
        Tourism = 23,
        
        [Description("Transport")]
        Transport = 24
    }

    public static class EnterpriseTypeEnumExtensions
    {
        public static string ToString(this EnterpriseType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
