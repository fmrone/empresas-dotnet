﻿using FR.ioasys.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FR.ioasys.Domain.Services
{
    public interface IEnterpriseService
    {
        Task<IEnumerable<Enterprise>> GetEnterprisesAsync();
        Task<Enterprise> GetEnterpriseByIdAsync(string id);
        Task<Enterprise> GetEnterpriseByNameAndTypeAsync(string name, byte type);
    }
}
