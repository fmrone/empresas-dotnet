﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FR.ioasys.Domain.Models
{
    public class Enterprise
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public byte Type { get; set; }
    }
}
