﻿using FR.ioasys.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FR.ioasys.Domain.Repositories
{
    public interface IEnterpriseReadOnlyRepository
    {
        Task<IEnumerable<Enterprise>> GetEnterprisesAsync();
        Task<Enterprise> GetEnterpriseByIdAsync(string id);
        Task<Enterprise> GetEnterpriseByNameAndTypeAsync(string name, byte type);
    }
}
