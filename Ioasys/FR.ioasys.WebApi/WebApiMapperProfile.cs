﻿using AutoMapper;
using FR.ioasys.Domain.Enumarators;
using FR.ioasys.Domain.Models;
using FR.ioasys.WebApi.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FR.ioasys.WebApi
{
    public class WebApiMapperProfile : Profile
    {
        public WebApiMapperProfile()
        {
            CreateMap<Enterprise, EnterpriseGetResult>()
                .ForMember(destino => destino.Id, opt => opt.MapFrom(origem => origem.Id))
                .ForMember(destino => destino.Name, opt => opt.MapFrom(origem => origem.Name))
                .ForMember(destino => destino.Type, opt => opt.MapFrom(origem => ((EnterpriseType)origem.Type).ToString()));
        }
    }
}
