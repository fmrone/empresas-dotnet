﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FR.ioasys.WebApi.Dto
{
    public class EnterpriseGet
    {
        public string Name { get; set; }
        public byte EnterpriseTypes { get; set; }
    }
}
