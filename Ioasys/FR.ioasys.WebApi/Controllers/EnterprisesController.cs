﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FR.ioasys.Domain.Models;
using FR.ioasys.Domain.Services;
using FR.ioasys.WebApi.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FR.ioasys.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Enterprises")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            _enterpriseService = enterpriseService ?? 
                throw new ArgumentNullException(nameof(enterpriseService));
        }

        // GET: api/Enterprises
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var enterprises = await _enterpriseService.GetEnterprisesAsync();
            var enterpriseGetResult = Mapper.Map<IEnumerable<Enterprise>, 
                IEnumerable<EnterpriseGetResult>>(enterprises);

            return Ok(enterpriseGetResult);
        }

        // GET: api/Enterprises/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(string id)
        {
            var enterprise = await _enterpriseService.GetEnterpriseByIdAsync(id);
            var enterpriseGetResult = Mapper.Map<Enterprise, 
                EnterpriseGetResult>(enterprise);

            return Ok(enterpriseGetResult);
        }

        [HttpGet("GetByNameAndEnterpriseTypes")]
        public async Task<IActionResult> GetByNameAndEnterpriseTypes([FromQuery]EnterpriseGet enterpriseGet)
        {
            var enterprise = await _enterpriseService
                .GetEnterpriseByNameAndTypeAsync(enterpriseGet.Name, 
                enterpriseGet.EnterpriseTypes);

            var enterpriseGetResult = Mapper.Map<Enterprise, 
                EnterpriseGetResult>(enterprise);

            return Ok(enterpriseGetResult);
        }
    }
}
