﻿using FR.ioasys.Domain.Models;
using FR.ioasys.Domain.Repositories;
using FR.ioasys.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FR.ioasys.Application.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseReadOnlyRepository _enterpriseReadOnlyRepository;

        public EnterpriseService(IEnterpriseReadOnlyRepository enterpriseReadOnlyRepository)
        {
            _enterpriseReadOnlyRepository = enterpriseReadOnlyRepository ?? 
                throw new ArgumentNullException(nameof(enterpriseReadOnlyRepository));
        }

        public async Task<IEnumerable<Enterprise>> GetEnterprisesAsync()
        {
            return await _enterpriseReadOnlyRepository.GetEnterprisesAsync();
        }

        public async Task<Enterprise> GetEnterpriseByIdAsync(string id)
        {
            return await _enterpriseReadOnlyRepository.GetEnterpriseByIdAsync(id);
        }

        public async Task<Enterprise> GetEnterpriseByNameAndTypeAsync(string name, byte type)
        {
            return await _enterpriseReadOnlyRepository.GetEnterpriseByNameAndTypeAsync(name, type);
        }
    }
}
